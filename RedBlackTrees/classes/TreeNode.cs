﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedBlackTrees.classes
{
    class TreeNode
    {
        // Data attribute and its getters/setters
        private int data;
        public int dataValue
        {
            get { return data; }
            set { data = value; }
        }
        
        // For Binary tree we have either left or right
        private TreeNode left;
        public TreeNode leftNode
        {
            get { return left; }
            set { left = value; }
        }

        // Right Node object and its getters/setters
        private TreeNode right;
        public TreeNode rightNode
        {
            get { return right; }
            set { right = value; }
        }

        // Parent Node, and its getters/setters
        private TreeNode parent;
        public TreeNode parentNode
        {
            get { return parent; }
            set { parent = value; }
        }
        
        
        public enum nodeColors
        {
            Black = 0,
            Red = 1
        }


        private nodeColors color;
        public nodeColors colorValue
        {
            get { return color; }
            set { color = value; }
        }
        

        /**
         * Constructor functions
         */
        public TreeNode(int value)
        {
            // Setting attributes of this class
            this.data = value;
            // Setting left and right to null, because the tree is empty initially
            this.left = this.right = null;
            this.color = nodeColors.Red;
        }

        public TreeNode(int value, TreeNode left, TreeNode right, nodeColors color)
        {
            // Setting attributes of this class
            this.data = value;
            // Setting left and right to null, because the tree is empty initially
            this.left = left;
            this.right = right;
            this.color = color;
        }

        /// <summary>
        /// For getting sibling of a node, if exists
        /// </summary>
        /// <returns>Sibling node|Null</returns>
        internal TreeNode getSibling()
        {
            if (this == null)
                return null;

            TreeNode sibling = null;
            if (this.parentNode != null)
                sibling = (this.parentNode.leftNode == this ? this.parentNode.rightNode : this.parentNode.leftNode);

            return sibling;
        }

        internal bool isLeftChild()
        {
            bool leftNode = false;
            if (this.parent != null)
                leftNode = (this.parent.left == this ? true : false);

            return leftNode;
        }

        internal TreeNode replaceWith(TreeNode nodeToReplace)
        {
            this.leftNode = nodeToReplace.leftNode;
            this.rightNode = nodeToReplace.rightNode;
            this.dataValue = nodeToReplace.dataValue;
            this.colorValue = nodeToReplace.colorValue;
            this.parentNode = nodeToReplace.parentNode;

            return this;
        }

        internal bool isRightChild()
        {
            bool rightNode = false;
            if (this.parent != null)
                rightNode = (this.parent.right == this ? true : false);

            return rightNode;
        }
    }
}
