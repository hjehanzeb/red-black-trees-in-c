﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedBlackTrees.classes
{
    class ApplicationManager
    {
        private RedBlackTree Tree;

        /// <summary>
        /// Constructor for ApplicationManager
        /// </summary>
        public ApplicationManager()
        {
            try
            {
                Tree = new RedBlackTree();

                // Displaying options in Menu 1 to select from
                displayFirstMenu();
            }
            catch (Exception ex)
            {
                Console.WriteLine("An error occurred in the program:\n" + ex.Message + "\n" + ex.StackTrace.ToString());
            }

        }

        /// <summary>
        /// Display the main menu with options to select
        /// </summary>
        private void displayFirstMenu()
        {
            Console.WriteLine("RED BLACK TREES\n" +
                              "-------------------------\n"
                );
            string choiceSelected = "";

            while (choiceSelected != "4")
            {
                Console.WriteLine("\nMain Menu\n" +
                                  "=============\n"
                    );
                Console.WriteLine("Enter from the choices below (1-4): ");
                Console.WriteLine("1. Insert into the tree");
                if (Tree.rootNode != null)
                {
                    Console.WriteLine("2. Remove a value");
                    Console.WriteLine("3. Display the tree");
                }
                Console.Write("4. Exit\n" +
                                  "Enter choice: "
                    );

                choiceSelected = Console.ReadLine();
                switch (choiceSelected)
                {
                    // Insert into the tree
                    case "1":
                        string valueToInsert = "";
                        while (valueToInsert != "q")
                        {
                            Console.Write("Enter value to insert ('q' to go back):\n");
                            valueToInsert = Console.ReadLine();
                            if (valueToInsert != "" && valueToInsert != "q")
                                Tree.insertNode(Convert.ToInt32(valueToInsert));
                            else
                                if (valueToInsert != "q")
                                    Console.Write("Nothing entered, please enter a value.\n");
                        }
                        break;

                    // Delete from the tree
                    case "2":
                        Console.Write("Enter value to delete:\n");
                        string valueToDelete = Console.ReadLine();
                        if (valueToDelete != "")
                            Tree.deleteNode(Convert.ToInt32(valueToDelete));
                        else
                            Console.Write("Nothing entered, please enter a value.\n");
                        break;

                    // Display the tree
                    case "3":
                        displayTraversalMenu();
                        break;

                    case "4":
                        return;
                }
            }
        }

        /// <summary>
        /// For displaying the trees, giving the user options to traverse them
        /// </summary>
        private void displayTraversalMenu()
        {
            string choiceSelected = "";

            while (choiceSelected != "4")
            {
                Console.Write("\nDisplay Tree Menu\n" +
                              "====================\n" +
                            "Enter in which order to display (1-3 or 4 to go back):\n" +
                            "1. In-order Traversal\n" +
                            "2. Pre-order Traversal\n" +
                            "3. Pre-order Traversal\n" +
                            "4. Back to Main Menu\n" +
                            "Enter choice: "
                            );

                TraverseTrees treeTraversal = new TraverseTrees();

                choiceSelected = Console.ReadLine();
                switch (choiceSelected)
                {
                    // Insert into the tree
                    case "1":
                        Console.WriteLine("Displaying the tree in In-Order:\n");
                        treeTraversal.inOrderTraverse(Tree.rootNode);
                        Console.WriteLine("\n");
                        break;

                    // Delete from the tree
                    case "2":
                        Console.Write("Displaying the tree in Pre-Order:\n");
                        treeTraversal.preOrderTraverse(Tree.rootNode);
                        
                        break;

                    // Display the tree
                    case "3":
                        Console.Write("Displaying the tree in Post-Order:\n");
                        treeTraversal.postOrderTraversal(Tree.rootNode);
                        
                        break;

                    case "4":
                        return;
                }
            }

        }
    }
}
