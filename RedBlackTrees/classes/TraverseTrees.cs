﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedBlackTrees.classes
{
    class TraverseTrees
    {
        public void preOrderTraverse(TreeNode node)
        {
            if (node == null)
            {
                return;
            }

            // Using recursive function to traverse
            if (node != null)
            {
                Console.Write(node.dataValue + " ");
                preOrderTraverse(node.leftNode);
                preOrderTraverse(node.rightNode);
            }
        }

        public void inOrderTraverse(TreeNode node)
        {
            if (node == null)
            {
                return;
            }

            // Using recursive function to traverse
            if (node != null)
            {
                inOrderTraverse(node.leftNode);
                Console.Write(node.dataValue + " ");
                inOrderTraverse(node.rightNode);
            }
            
        }

        public void postOrderTraversal(TreeNode node)
        {
            if (node == null)
            {
                return;
            }

            // Using recursive function to traverse
            if (node != null)
            {
                postOrderTraversal(node.leftNode);
                postOrderTraversal(node.rightNode);
                Console.Write(node.dataValue + " ");
            }
        }
    }
}
