﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedBlackTrees.classes
{
    /// <summary>
    /// Class for Binary Search Tree, containing functions to insert, find or delete data
    /// </summary>
    class BinarySearchTree
    {
        // Root attribute and its getter/setter
        protected TreeNode root;
        public TreeNode rootNode
        {
            get { return root; }
            set { root = value; }
        }

        /// <summary>
        /// For inserting node in another way, without recursion
        /// </summary>
        /// <param name="root">Root Node of the tree</param>
        /// <param name="value">value to be inserted</param>
        public TreeNode insertNode(int value)
        {
            try
            {
                TreeNode newNode = new TreeNode(value);
                TreeNode tempNode = null;
                TreeNode currentNode = root;

                // Traversing the tree from root, and inserting the value where it's appropriate
                tempNode = findInsertionAppropriateNode(value);

                // We are now in a place to insert our new node, but see if the tempNode is null. This case will only occur if tree is empty
                newNode.parentNode = tempNode;

                // If tempNode is null, tree is empty, set root to newNode (with our value being inserted)
                if (tempNode == null)
                {
                    root = newNode;
                }
                // If our value to be inserted is lesser than parent node, then our newNode will go to the left
                else if (newNode.dataValue < tempNode.dataValue)
                    tempNode.leftNode = newNode;
                // otherwise it will go to the right of parent node (tempNode)
                else
                    tempNode.rightNode = newNode;

                return newNode;
            }
            catch (Exception ex)
            {
                Console.WriteLine("An error occurred in the program:\n" + ex.Message + "\n" + ex.StackTrace.ToString());
            }

            return null;
        }

        /// <summary>
        /// Find the node where the new node can be inserted as child
        /// </summary>
        /// <param name="value">The value to be inserted</param>
        /// <returns>Node where the new node can be inserted as a child</returns>
        private TreeNode findInsertionAppropriateNode(int value)
        {
            TreeNode currentNode = root;
            TreeNode tempNode = null;
            // Traversing the tree, to find the place for insertion of new value
            while (currentNode != null)
            {
                // Setting tempNode to currentNode to store the value when needed later
                tempNode = currentNode;
                if (value < currentNode.dataValue)
                {
                    currentNode = currentNode.leftNode;
                }
                else
                {
                    currentNode = currentNode.rightNode;
                }
            }

            // returning the parent of new node
            return tempNode;
        }

        /// <summary>
        /// To find/locate the node of a given value
        /// </summary>
        /// <param name="value">value to be find in the tree</param>
        /// <returns>Node|Null depending if it was found or not</returns>
        protected TreeNode findNode(int value)
        {
            TreeNode currentNode = root;
            TreeNode tempNode = null;
            // Traversing the tree, to find the place for insertion of new value
            while (currentNode != null)
            {
                // Setting tempNode to currentNode to store the value when needed later
                tempNode = currentNode;
                if (value < currentNode.dataValue)
                {
                    currentNode = currentNode.leftNode;
                }
                else
                {
                    currentNode = currentNode.rightNode;
                }

                // If value is found, break the loop
                if (tempNode.dataValue == value)
                    break;
            }

            // returning the parent of new node
            return tempNode;
        }

        /// <summary>For deleting a value or node from the BSTree</summary>
        /// <param name="value">The data value of Node to be deleted from the Tree</param>
        /// <remarks>Returns nothing</remarks>
        public TreeNode deleteNode(int valueToDelete)
        {
            try
            {
                TreeNode node = root;
                TreeNode nodeToDelete = null;
                TreeNode tempNode = null;
                TreeNode successorNode = null;

                if (node != null)
                {
                    // Before going further, check if the node to be removed is root?
                    if (root.dataValue == valueToDelete)
                    {
                        nodeToDelete = root;
                    }
                    else
                    {
                        // Traversing the tree from root, finding the node to delete
                        TreeNode searchedNode = findNode(valueToDelete);
                        if (searchedNode.dataValue == valueToDelete)
                            // We have found the node
                            nodeToDelete = searchedNode;
                        else
                            // No node was found with this data (valueToDelete), so return null
                            return null;
                    }

                    // If nodeToDelete's children are both null, point it's parent to null and return that node
                    if (nodeToDelete.leftNode == null && nodeToDelete.rightNode == null)
                    {
                        // If left child of parent, point leftchild to null, else point right child to null
                        if (nodeToDelete.parentNode.leftNode == nodeToDelete)
                            nodeToDelete.parentNode.leftNode = null;
                        else
                            nodeToDelete.parentNode.rightNode = null;

                        return nodeToDelete;
                    }
                    
                    // If nodeToDelete has one child only, splice the child with its parent
                    if (nodeToDelete.leftNode == null || nodeToDelete.rightNode == null)
                    {
                        if (nodeToDelete.parentNode != null)
                        {
                            tempNode = nodeToDelete.leftNode == null ? nodeToDelete.rightNode : nodeToDelete.leftNode;
                            if (nodeToDelete.isLeftChild())
                                nodeToDelete.parentNode.leftNode = tempNode;
                            else
                                nodeToDelete.parentNode.rightNode = tempNode;
                        }
                    }
                    // NodeToDelete has both children not null, so set successor of node to its parent
                    else
                    {
                        successorNode = findSuccessor(nodeToDelete);
                        // tempNodeChild will have at most one child
                        // Instead of deleting we can just copy the successor's data over to the node to be deleted
                        nodeToDelete.dataValue = successorNode.dataValue;
                        nodeToDelete.colorValue = successorNode.colorValue;
                        nodeToDelete.rightNode = successorNode.rightNode;

                        // Now delete the successor and set its child (if any) to its parent
                        tempNode = successorNode.leftNode == null ? successorNode.rightNode : successorNode.leftNode;
                        tempNode.parentNode = successorNode.parentNode;
                        if (successorNode.leftNode != null)
                        {
                            if (successorNode.isLeftChild())
                                successorNode.parentNode.leftNode = tempNode;
                            else
                                successorNode.parentNode.rightNode = tempNode;
                        }
                        else
                        {
                            if (successorNode.isLeftChild())
                                successorNode.parentNode.leftNode = tempNode;
                            else
                                successorNode.parentNode.rightNode = tempNode;
                        }
                    }

                    return successorNode;

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("An error occurred in the program:\n" + ex.Message + "\n" + ex.StackTrace.ToString());
            }

            return null;
        }

        /// <summary>
        /// For finding the successor of the node. Basically successor of the node is the Next-node in InOrderTraversal
        /// </summary>
        /// <param name="node">The node for which we need to find the successor</param>
        /// <returns>Successor node</returns>
        private TreeNode findSuccessor(TreeNode node)
        {
            // Node is null, return null
            if (node == null)
                return null;

            // If node has a right child != null, then we have the successor, minimum node, in the right tree of Node
            if (node.rightNode != null)
                return findMinimum(node.rightNode);

            // Otherwise, we have to find next-node/successor in parents or siblings
            TreeNode parent = node.parentNode;
            TreeNode tempNode = node;
            // As we traverse left up the tree we traverse smaller values so the first node on the right
            // is the next larger number
            while (parent != null && tempNode == parent.rightNode)
            {
                tempNode = parent;
                parent = parent.parentNode;
            }

            return parent;
        }

        /// <summary>
        /// To find the minimum in the whole tree starting from the root
        /// </summary>
        /// <param name="node">Root node of the tree/sub tree</param>
        /// <returns>The minimum in the Tree</returns>
        private TreeNode findMinimum(TreeNode node)
        {
            // If Node is null, return null
            if (node == null)
                return node;

            TreeNode tempNode = node;

            // Loop to the left until we hit null. Parent of that null will be the minimum
            while (tempNode != null)
            {
                // If node in the loop has left-child of null, break out of the loop, as we have found the
                // minimum
                if (tempNode.leftNode == null)
                    break;
                // else keep moving to the left
                else
                    tempNode = tempNode.leftNode;
            }

            // Return node with minimum value
            return tempNode;
        }

        /// <summary>
        /// The following steps are involved in Left rotation of a node
        /// 1. node has a right child, which becomes node's parent
        /// 2. parent's left child becomes node's new right child
        /// 3. Node's parent will be new parent's parent now.
        /// </summary>
        /// <param name="node">The node to be left rotated</param>
        protected void leftRotate(ref TreeNode node)
        {
            // Check that node has a right child
            if (node.rightNode != null)
            {
                TreeNode rightChild = node.rightNode;
                node.rightNode = rightChild.leftNode;
                if (rightChild.leftNode != null)
                    rightChild.leftNode.parentNode = node;
                rightChild.parentNode = node.parentNode;

                if (node.parentNode == null)
                    root = rightChild;
                // If node is left child of its parent
                else if (node == node.parentNode.leftNode)
                    node.parentNode.leftNode = rightChild;
                else
                    node.parentNode.rightNode = rightChild;

                rightChild.leftNode = node;
                node.parentNode = rightChild;
            }
        }

        /// <summary>
        /// The following steps are involved in right rotation
        /// 1. Node has a left child, which becomes its parent
        /// 2. New parent's right child will be node
        /// 3. Node's parent will be new parent's parent now.
        /// </summary>
        /// <param name="Node">The node to be right rotated</param>
        protected void rightRotate(ref TreeNode node)
        {
            // Check that node has a right child
            if (node.leftNode != null)
            {
                TreeNode leftChild = node.leftNode;
                node.leftNode = leftChild.rightNode;
                if (leftChild.rightNode != null)
                    leftChild.rightNode.parentNode = node;
                leftChild.parentNode = node.parentNode;

                // If node's parent was null, it was root, new root is the Left Child now
                if (node.parentNode == null)
                    root = leftChild;
                // If node is right child of its parent
                else if (node == node.parentNode.rightNode)
                    node.parentNode.rightNode = leftChild;
                else
                    node.parentNode.leftNode = leftChild;

                leftChild.rightNode = node;
                node.parentNode = leftChild;
            }
        }
    }
}
