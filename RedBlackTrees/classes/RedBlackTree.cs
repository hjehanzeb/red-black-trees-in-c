﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedBlackTrees.classes
{
    /// <summary>
    /// Red-Black tree class having functions to Add and Delete
    /// </summary>
    class RedBlackTree : BinarySearchTree
    {       
        /// <summary>Constructors for RedBlackTree class</summary>
        public RedBlackTree()
        {
            base.root = null;
        }

        public RedBlackTree(TreeNode rootNode)
        {
            base.root = rootNode;
        }

        /// <summary>For inserting the data in the RBTree</summary>
        /// <param name="value">The value to be inserted</param>
        /// <remarks>Returns void</remarks>
        public void insertNode(int value)
        {
            try
            {
                TreeNode newNode = null;
                newNode = base.insertNode(value);

                // If New node is inserted successfully
                if (newNode != null)
                {
                    // Now that we have inserted the node in a place it has to be in simple BST, we need to see the Color value and fix it a bit
                    // so that it's proper RBT insertion
                    insertFix(newNode);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("An error occurred in the program:\n" + ex.Message + "\n" + ex.StackTrace.ToString());
            }
        }
        /// <summary>
        /// This function will fix the red/black properties of the nodes
        /// </summary>
        /// <param name="newNode">The new node, that's inserted in the Tree in insertNode functions</param>
        private void insertFix(TreeNode newNode)
        {
            // y = parentsRightNode
            // z = newNode
            TreeNode newNodeRightUncle;
            TreeNode newNodeLeftUncle;
            TreeNode grandParent = null;

            // If newNode's parent is null, node is at the root, so just return out of the function
            if (newNode.parentNode == null)
                return;

            // Loop starting from new node inserted, until we have reached the grandparent Red node
            while (newNode != root && newNode.parentNode.colorValue == TreeNode.nodeColors.Red)
            {
                // Setting grandparent
                grandParent = newNode.parentNode.parentNode;
                if (grandParent == null)
                    return;

                // If the newNode's parent node is at the left side
                if (newNode.parentNode.isLeftChild())
                {
                    // Node Right uncle
                    newNodeRightUncle = grandParent.rightNode;

                    // If the new node's parent's right node has color bit of Red, then make parent's node color to Black and
                    // new node's color to Black as well. This is basically bringing down the black from grandparent
                    if (newNodeRightUncle != null && newNodeRightUncle.colorValue == TreeNode.nodeColors.Red)
                    {
                        // Setting parent's color to Black
                        newNode.parentNode.colorValue = TreeNode.nodeColors.Black;
                        // Setting parent's right node's color to Black
                        newNodeRightUncle.colorValue = TreeNode.nodeColors.Black;
                        // Setting parent's parent color to Red
                        grandParent.colorValue = TreeNode.nodeColors.Red;

                        // Setting the node to Grandparent, taking it as the newNode and fixing colors if required, that is,
                        // if Grandparent's parent is Red
                        newNode = grandParent;
                    }
                    // If color is black
                    else
                    {
                        if (newNode.isRightChild())
                        {
                            newNode = newNode.parentNode;
                            leftRotate(ref newNode);
                        }
                        newNode.parentNode.colorValue = TreeNode.nodeColors.Black;
                        grandParent.colorValue = TreeNode.nodeColors.Red;

                        // Right rotate the new node's grandparent
                        rightRotate(ref grandParent);
                    }
                }
                // Parent Node is to the right of its parent
                else
                {
                    // Setting the left node of newNode's parent as tempNode
                    newNodeLeftUncle = grandParent.leftNode;

                    // If the new node's parent's right node has color bit of Red, then make parent's node color to Black and
                    // new node's color to Black as well. This is basically bringing down the black from grandparent
                    if (newNodeLeftUncle != null && newNodeLeftUncle.colorValue == TreeNode.nodeColors.Red)
                    {
                        // Setting parent's color to Black
                        newNode.parentNode.colorValue = TreeNode.nodeColors.Black;
                        // Setting parent's right node's color to Black
                        newNodeLeftUncle.colorValue = TreeNode.nodeColors.Black;
                        // Setting parent's parent color to Red
                        grandParent.colorValue = TreeNode.nodeColors.Red;
                        newNode = grandParent;
                    }
                    // If new node is inserted on the right, then
                    else if (newNode.isRightChild())
                    {
                        newNode = newNode.parentNode;
                        leftRotate(ref newNode);
                        newNode.parentNode.colorValue = TreeNode.nodeColors.Black;
                        grandParent.colorValue = TreeNode.nodeColors.Red;

                        // Right rotate the new node's grandparent
                        rightRotate(ref grandParent);
                    }

                }
            }

            root.colorValue = TreeNode.nodeColors.Black;
        }

        /// <summary>For deleting a value or node from the Tree</summary>
        /// <param name="value">The data value of Node to be deleted from the Tree</param>
        /// <remarks>Returns nothing</remarks>
        public TreeNode deleteNode(int value)
        {
            try
            {
                // Delete node as done in normal BST
                TreeNode nodeToDelete = base.deleteNode(value);

                // if node-being-removed's color is black, then we need to fix the tree as Red-Black property is disturbed
                if (nodeToDelete.colorValue == TreeNode.nodeColors.Black)
                    deleteNodeFix(nodeToDelete);

                return nodeToDelete;
            }
            catch (Exception ex)
            {
                Console.WriteLine("An error occurred in the program:\n" + ex.Message + "\n" + ex.StackTrace.ToString());
            }

            return null;
        }
        /// <summary>
        /// For fixing the Coloring of the tree, after a node is removed
        /// </summary>
        /// <param name="node">The node that replaced the deleted node</param>
        private void deleteNodeFix(TreeNode node)
        {
            TreeNode siblingNode = null;
            while (node != root && node.colorValue == TreeNode.nodeColors.Black) {
                // If node is the left child of its parent
                if (node.isLeftChild())
                {
                    siblingNode = node.getSibling();

                    if (siblingNode.colorValue == TreeNode.nodeColors.Red)
                    {
                        node.parentNode.colorValue = TreeNode.nodeColors.Red;
                        siblingNode.colorValue = TreeNode.nodeColors.Black;

                        TreeNode nodeParent = node.parentNode;
                        leftRotate(ref nodeParent);
                        // The new sibling of node
                        siblingNode = node.getSibling();
                    }

                    if (siblingNode.leftNode.colorValue == TreeNode.nodeColors.Black && siblingNode.rightNode.colorValue == TreeNode.nodeColors.Black)
                    {
                        siblingNode.colorValue = TreeNode.nodeColors.Red;
                        node = node.parentNode;
                    }
                    else
                    {
                        if (siblingNode.rightNode.colorValue == TreeNode.nodeColors.Black)
                        {
                            siblingNode.leftNode.colorValue = TreeNode.nodeColors.Black;
                            siblingNode.colorValue = TreeNode.nodeColors.Red;
                            rightRotate(ref siblingNode);
                            siblingNode = node.parentNode.rightNode;
                        }
                        siblingNode.colorValue = node.parentNode.colorValue;
                        node.parentNode.colorValue = TreeNode.nodeColors.Black;
                        siblingNode.rightNode.colorValue = TreeNode.nodeColors.Black;

                        TreeNode nodeParent = node.parentNode;
                        leftRotate(ref nodeParent);

                        node = root;
                    }
                }
                else
                {
                    siblingNode = node.getSibling();

                    if (siblingNode.colorValue == TreeNode.nodeColors.Red)
                    {
                        siblingNode.colorValue = TreeNode.nodeColors.Black;
                        node.parentNode.colorValue = TreeNode.nodeColors.Red;

                        TreeNode nodeParent = node.parentNode;
                        leftRotate(ref nodeParent);
                        siblingNode = node.getSibling();
                        // If after rotation we have sibling = null, then node is now root, or doesn't have any sibling
                        if (siblingNode == null)
                            return;
                    }

                    if (siblingNode.leftNode != null && siblingNode.rightNode != null && siblingNode.leftNode.colorValue == TreeNode.nodeColors.Black && siblingNode.rightNode.colorValue == TreeNode.nodeColors.Black)
                    {
                        siblingNode.colorValue = TreeNode.nodeColors.Red;
                        node = node.parentNode;
                    }
                    else
                    {
                        if (siblingNode.leftNode != null && siblingNode.leftNode.colorValue == TreeNode.nodeColors.Black)
                        {
                            siblingNode.rightNode.colorValue = TreeNode.nodeColors.Black;
                            siblingNode.colorValue = TreeNode.nodeColors.Red;
                            rightRotate(ref siblingNode);
                            siblingNode = node.parentNode.leftNode;
                        }
                        siblingNode.colorValue = node.parentNode.colorValue;
                        node.parentNode.colorValue = TreeNode.nodeColors.Black;
                        
                        if (siblingNode.rightNode != null)
                            siblingNode.rightNode.colorValue = TreeNode.nodeColors.Black;

                        TreeNode nodeParent = node.parentNode;
                        leftRotate(ref nodeParent);

                        node = root;
                    }
                }
            }
        }

        /// <summary>
        /// Function for updating a value already present
        /// </summary>
        /// <param name="oldValue">The value to be updated</param>
        /// <param name="newValue">The new value to replace the older value</param>
        public void updateNode(int oldValue, int newValue)
        {
            try
            {
                // Finding, if old value exists
                TreeNode oldValueNode = base.findNode(oldValue);

                if (oldValueNode != null)
                {
                    // Calling insert function to insert the node, once inserted, we will call deleteNode function
                    // to remove the older value
                    this.insertNode(newValue);

                    // Removing the value to be updated, as new value has already been inserted into tree
                    this.deleteNode(oldValue);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("An error occurred in the program:\n" + ex.Message + "\n" + ex.StackTrace.ToString());
            }
        }
    }
}
